$(function(){
  //点击"注册"的链接
  $('#link_register').on('click',function () {
    $('.login-box').hide()
    $('.register-box').show()
  })
   //点击"登录"的链接
   $('#link_login').on('click',function () {
    $('.login-box').show()
    $('.register-box').hide()
  })

  var form =layui.form
  form.verify({
    pass: [
      /^[\S]{6,12}$/
      ,'密码必须6到12位，且不能出现空格'
    ],
    repwd:function(value) {
      var pwd=$("#form_reg [name=password]" ).val()
      if (pwd !== value) {
        return '两次密码不一致！'
      }
    }
  })

  var layer =layui.layer

  //监听注册表单的提交事件
  $("#form_reg").on("submit",function(e){
    e.preventDefault()
    var data={
      username:$("#form_reg [name=username]" ).val(),
      password:$("#form_reg [name=password]" ).val()
    }
    $.post("/api/reguser",data,function(res){
      if (res.status !== 0) {
        return layer.msg(res.message)
      }
   layer.msg('注册成功，请登录！')
      // 模拟人的点击行为docker
      $('#link_login').click()
    })
  })

  //监听登录表单的提交事件
  $('#form_login').submit(function(e){
    e.preventDefault()
    $.ajax({
      url:'/api/login',
      type:'POST',
      data:$(this).serialize(),
      success:function(res){
        if (res.status !== 0) {
          return layer.msg('登录失败！')
        }
        layer.msg('登录成功！')
        // 将登录成功得到的 token 字符串，保存到 localStorage 中
        localStorage.setItem('token', res.token)
        // 跳转到后台主页
        location.href = '/index.html'
      }

    })
  })


})